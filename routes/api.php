<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CategoryController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/category','\App\Http\Controllers\CategoryController@allCats');
Route::get('/category/dom','\App\Http\Controllers\CategoryController@index');
Route::get('/category/dom/sorted','\App\Http\Controllers\CategoryController@orderedList');
Route::get('/category/{id}/edit','\App\Http\Controllers\CategoryController@editHandler');
Route::post('/category/{id}/submit','\App\Http\Controllers\CategoryController@submitHandler');
Route::get('/category/{id}','\App\Http\Controllers\CategoryController@show');
Route::get('/createCategory','\App\Http\Controllers\CategoryController@createHandler');
Route::post('/ccategory','\App\Http\Controllers\CategoryController@store');
Route::delete('/category/{id}','\App\Http\Controllers\CategoryController@destroy');

// Route::prefix('category')->group(function () {
//     Route::get('/',[CategoryController::class,'allCats']);
//     Route::get('/dom',[CategoryController::class,'index']);
//     Route::get('/dom/sorted',[CategoryController::class,'orderedList']);
//     Route::get('/{id}/edit',[CategoryController::class,'editHandler']);
//     Route::post('/{id}/submit',[CategoryController::class,'submitHandler']);
//     Route::get('/{id}',[CategoryController::class,'show']);
//     Route::get('/create',[CategoryController::class,'createHandler']);
//     Route::post('/category',[CategoryController::class],'store');
//     Route::delete('/{id}',[CategoryController::class],'destroy');
// });