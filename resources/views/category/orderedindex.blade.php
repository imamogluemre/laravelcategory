@foreach($parentCategories->sortBy('title') as $taxonomy)
<ul>
    <li><a href="">{{$taxonomy->title}}</a></li>
    @if(count($taxonomy->subcategory))
    @include('category.orderedsub',['subcategories' => $taxonomy->subcategory])
    @endif
</ul>
@endforeach