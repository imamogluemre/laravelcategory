@foreach($subcategories->sortBy('title') as $subcategory)
<ul>
    <li>{{$subcategory->title}}</li>
    @if(count($subcategory->subcategory))
    @include('category.orderedsub',['subcategories' => $subcategory->subcategory])
    @endif
</ul>
@endforeach