<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $markup = '';
        $parentCategories = \App\Models\Category::where('parent_id', NULL)->get();
        echo ($this->treeOut($parentCategories,$markup));
    }
    public function treeOut(&$parentCategories,&$markup)
    {
        
        foreach ($parentCategories as $pc) {
            $markup .= '<li>';
            $markup .= $pc->title;

            if($pc->children){
                $markup .= '<ul>';
                $this->treeOut($pc->children,$markup);
                $markup .= '</ul>';
            }
            $markup .= '</li>';
            
        }
        return $markup;

        // foreach ($parentCategories as $category) {
        //     echo '<li>';
        //     echo $category->title;
        //     if ($category->children) {
        //         echo '<ul>';
        //         $this->treeOut($category->children);
        //         echo '</ul>';
        //     }
        //     echo '</li>';
        // }
    }
    public function orderedTree(&$parentCategories,&$markup){
        // foreach ($parentCategories->sortBy('title') as $category) {
        //     echo '<li>';
        //     echo $category->title;
        //     if ($category->children) {
        //         echo '<ul>';
        //         $this->orderedTree($category->children);
        //         echo '</ul>';
        //     }
        //     echo '</li>';
        // }

        foreach ($parentCategories->sortBy('title') as $pc) {
            $markup .= '<li>';
            $markup .= $pc->title;

            if($pc->children){
                $markup .= '<ul>';
                $this->orderedTree($pc->children,$markup);
                $markup .= '</ul>';
            }
            $markup .= '</li>';
            
        }
        return $markup;
    }

    public function allCats()
    {
        $cats = Category::all();
        return $cats;
    }


    public function editHandler($id)
    {
        $cat = Category::findOrFail($id);
        return view('category.edit', compact('cat'));
    }

    public function submitHandler(Request $request, $id)
    {

        $category = Category::findOrFail($id);
        $category->parent_id = $request->input('catParentId');
        $category->title = $request->input('catTitle');
        if ($category->save()) {
            return response()->json('Ok');
        }
    }
    public function createHandler()
    {
        return view('category.categoryform');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category();
        $category->parent_id = $request->input('pid');
        $category->title = $request->input('cname');
        //$category->save();
        if ($category->save()) {
            return response()->json('Ok');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::findOrFail($id);
        return $category;
    }

    public function orderedList()
    {
        $markup = "";
        $parentCategories = \App\Models\Category::where('parent_id', NULL)->get();
        echo ($this->orderedTree($parentCategories,$markup));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $category->parent_id = $request->input('parent_id');
        $category->title = $request->input('title');
        if ($category->save()) {
            return response()->json('Ok');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::findOrFail($id);
        if ($category->delete()) {
            return response()->json('Ok');
        }
    }
}
