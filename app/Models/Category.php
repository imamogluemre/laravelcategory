<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Category extends Model
{
    use HasFactory;
    use NodeTrait;

    protected $guarded = [];
    protected $fillable = ['title','parent_id'];

    public $timestamps = false;
    public function subcategory()
    {

        return $this->hasMany('App\Models\Category', 'parent_id');
    }
}
